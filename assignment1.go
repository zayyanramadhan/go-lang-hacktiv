package main

import (
	"fmt"
	"os"
	"strconv"
)

type dataku struct {
	nama      string
	alamat    string
	pekerjaan string
	alasan    string
}

func main() {
	var dataku = []dataku{
		{nama: "Zayyan", alamat: "alamat Zayyan", pekerjaan: "pekerjaan Zayyan", alasan: "alasan Zayyan"},
		{nama: "Fajar", alamat: "alamat Fajar", pekerjaan: "pekerjaan Fajar", alasan: "alasan Fajar"},
		{nama: "Zaka", alamat: "alamat Zaka", pekerjaan: "pekerjaan Zaka", alasan: "alasan Zaka"},
		{nama: "Hendar", alamat: "alamat Hendar", pekerjaan: "pekerjaan Hendar", alasan: "alasan Hendar"},
		{nama: "Billy", alamat: "alamat Billy", pekerjaan: "pekerjaan Billy", alasan: "alasan Billy"},
		{nama: "Rizki", alamat: "alamat Rizki", pekerjaan: "pekerjaan Rizki", alasan: "alasan Rizki"},
	}

	if len(os.Args) > 1 {
		num, _ := strconv.Atoi(os.Args[1])
		if num < 6 {
			cetak(*&dataku[num].nama)
			cetak(*&dataku[num].alamat)
			cetak(*&dataku[num].pekerjaan)
			cetak(*&dataku[num].alasan)
		} else {
			fmt.Printf("data tidak ada!!")
		}
	} else {
		fmt.Printf("belum input")
	}
}

func cetak(print string) {
	fmt.Printf("%+v\n", print)
}
