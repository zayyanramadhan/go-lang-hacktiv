package main

import "fmt"

func main() {
	Zayyan := "Zayyan"
	Fajar := "Fajar"
	Hendar := "Hendar"
	Billy := "Billy"
	Zaka := "Zaka"
	Rizki := "Rizki"
	Ramadhan := "Ramadhan"
	Maulana := "Maulana"
	Tambunan := "Tambunan"
	Mahmud := "Mahmud"
	var nama = []*string{&Zayyan, &Fajar, &Hendar, &Billy, &Zaka, &Rizki, &Ramadhan, &Maulana, &Tambunan, &Mahmud}

	cetak := func(data []*string) {
		for _, v := range data {
			fmt.Println(*v)
		}
	}

	cetak(nama)

}
