package main

import "fmt"

func main() {
	var nama = make([]string, 0)
	nama = append(nama, "Zayyan", "Fajar", "Hendar", "Billy", "Zaka", "Rizki", "Ramadhan", "Maulana", "Tambunan", "Mahmud")

	for _, n := range nama {
		cetak(n)
	}

}

func cetak(print string) {
	fmt.Println(print)
}
