package main

import "fmt"

type User struct {
	Name string
}

type DataUser struct {
	data []*User
}

type Register interface {
	SimpanUser(u *User) string
	Tampilkandata() []*User
}

type Getuser interface {
	Tampilkandata() []*User
}

func (data *DataUser) SimpanUser(user *User) string {
	data.data = append(data.data, user)
	return user.Name
}

func (data *DataUser) Tampilkandata() []*User {
	return data.data
}

func NewUserService(data []*User) Register {
	return &DataUser{
		data: data,
	}
}

func main() {
	var data []*User
	userData := NewUserService(data)
	test := userData.SimpanUser(&User{Name: "zayyan"})
	test = userData.SimpanUser(&User{Name: "ramadhan"})
	fmt.Println(test)
	testget := userData.Tampilkandata()

	for _, value := range testget {
		fmt.Printf("%+v\n", *value)
	}

}
