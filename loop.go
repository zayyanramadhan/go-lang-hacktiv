package main

import "fmt"

func main() {
	fmt.Println("Assignment Condition and Looping")

	count := 10
	for i := 1; i <= count; i++ {
		if i%2 == 0 {
			fmt.Println(i, " Genap")
		} else {
			fmt.Println(i, " Ganjil")
		}
	}
}
