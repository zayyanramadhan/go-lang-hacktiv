package main

import "fmt"

func main() {

	var nama = make([]string, 0)
	nama = append(nama, "Zayyan", "Fajar", "Hendar", "Billy", "Zaka", "Rizki", "Ramadhan", "Maulana", "Tambunan", "Mahmud")

	for i, n := range nama {
		fmt.Println(i, n)
	}

}
