package main

import "fmt"

type Listnama struct {
	nama string
}

func main() {
	Zayyan := "Zayyan"
	Fajar := "Fajar"
	Hendar := "Hendar"
	Billy := "Billy"
	Zaka := "Zaka"
	Rizki := "Rizki"
	Ramadhan := "Ramadhan"
	Maulana := "Maulana"
	Tambunan := "Tambunan"
	Mahmud := "Mahmud"
	var nama = []*Listnama{{nama: Zayyan}, {nama: Fajar}, {nama: Hendar}, {nama: Billy}, {nama: Zaka}, {nama: Rizki}, {nama: Ramadhan}, {nama: Maulana}, {nama: Tambunan}, {nama: Mahmud}}

	cetak := func(data []*Listnama) {
		for _, v := range data {
			fmt.Println(v.nama)
		}
	}

	cetak(nama)

}
